package net.amar9059.ap_androidapp.Retrofit;

import net.amar9059.ap_androidapp.Model.NextMatch.ResponseNextMatch;
import net.amar9059.ap_androidapp.Model.Players.ResponsePlayers;
import net.amar9059.ap_androidapp.Model.PrevMatch.ResponsePrevMatch;
import net.amar9059.ap_androidapp.Model.Teams.ResponseTeams;
import net.amar9059.ap_androidapp.Model.Teams.Teams;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BaseApiService {

    @GET("search_all_teams.php")
    Call<ResponseTeams> getTeam (@Query("l") String leaguess);

    @GET("eventspastleague.php")
    Call<ResponsePrevMatch> getPrevMatch (@Query("id") String idLeague);

    @GET("eventsnextleague.php")
    Call<ResponseNextMatch> getNextMatch (@Query("id") String idLeagueNext);

    @GET("lookup_all_players.php")
    Call<ResponsePlayers> getPlayers (@Query("id") String idTeam);

}
