package net.amar9059.ap_androidapp.View.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;

import net.amar9059.ap_androidapp.Model.Teams.Teams;
import net.amar9059.ap_androidapp.Others.LocaleHelper;
import net.amar9059.ap_androidapp.R;
import net.amar9059.ap_androidapp.View.fragment.FavoritesFragment;
import net.amar9059.ap_androidapp.View.fragment.MatchesFragment;
import net.amar9059.ap_androidapp.View.fragment.TeamsFragment;
import net.amar9059.ap_androidapp.data.AppPreference;
import net.amar9059.ap_androidapp.data.TeamsHelper;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private MatchesFragment matchesFragment;
    private TeamsFragment teamsFragment;
    private FavoritesFragment favoritesFragment;
    private BottomNavigationView bottomNavigationView;
    private Toolbar toolbar;
    Locale locale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Football App");

        matchesFragment = new MatchesFragment();
        teamsFragment = new TeamsFragment();
        favoritesFragment = new FavoritesFragment();

        if (savedInstanceState == null){
            setFragment(matchesFragment);
        }

        bottomNavigationView = findViewById(R.id.bottom_navigation_bar);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.matches:
                        setFragment(matchesFragment);
                        return true;
                    case R.id.teams:
                        setFragment(teamsFragment);
                        return true;
                    case R.id.favorites:
                        setFragment(favoritesFragment);
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.language_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.language_english:
                updateViews("EN");
                break;
            case R.id.langauge_indonesia:
                updateViews("ID");
                break;
                default:
                    updateViews("EN");
                    break;
        }
        return true;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    private void updateViews(String languageCode) {
        Context context = LocaleHelper.setLocale(this, languageCode);
        Resources resources = context.getResources();

//        BottomNavigationView.se
//
//
//        mTitleTextView.setText(resources.getString(R.string.main_activity_title));
//        mDescTextView.setText(resources.getString(R.string.main_activity_desc));
//        mAboutTextView.setText(resources.getString(R.string.main_activity_about));
//        mToTRButton.setText(resources.getString(R.string.main_activity_to_tr_button));
//        mToENButton.setText(resources.getString(R.string.main_activity_to_en_button));
//
//        setTitle(resources.getString(R.string.main_activity_toolbar_title));
    }

    private class LoadData extends AsyncTask<Void, Integer, Void> {
        final String TAG = LoadData.class.getSimpleName();
        TeamsHelper teamsHelper;
        AppPreference appPreference;
        double progress;
        double maxprogress = 100;

        @Override
        protected void onPreExecute() {

            teamsHelper = new TeamsHelper(MainActivity.this);
            appPreference = new AppPreference(MainActivity.this);
        }

        @Override
        protected Void doInBackground(Void... params) {

            Boolean firstRun = appPreference.getFirstRun();

            if (firstRun) {

                ArrayList<Teams> teams = preLoadRaw();

                teamsHelper.open();

                progress = 30;
                publishProgress((int) progress);
                Double progressMaxInsert = 80.0;
                Double progressDiff = (progressMaxInsert - progress) / teams.size();

                for (Teams model : teams) {
                    teamsHelper.insert(model);
                    progress += progressDiff;
                    publishProgress((int)progress);
                }

                teamsHelper.close();

                appPreference.setFirstRun(false);

                publishProgress((int) maxprogress);

            } else {
                try {
                    synchronized (this) {
                        this.wait(2000);

                        publishProgress(50);

                        this.wait(2000);
                        publishProgress((int) maxprogress);
                    }
                } catch (Exception e) {
                }
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            Intent i = new Intent(MainActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        }
    }

    public ArrayList<Teams> preLoadRaw() {
        ArrayList<Teams> teamsArrayList = new ArrayList<>();
        String line = null;
        BufferedReader reader;
        try {
            Resources res = getResources();
//            InputStream raw_dict = res.openRawResource(R.raw.data_mahasiswa);
//
//            reader = new BufferedReader(new InputStreamReader(raw_dict));
            int count = 0;
            do {
                //line = reader.readLine();

                Teams teams;

                teams = new Teams();
                teamsArrayList.add(teams);
                count++;
            } while (line != null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return teamsArrayList;
    }


}
