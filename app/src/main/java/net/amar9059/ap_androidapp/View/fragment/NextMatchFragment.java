package net.amar9059.ap_androidapp.View.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import net.amar9059.ap_androidapp.Adapter.CustomAdapterNextMatch;
import net.amar9059.ap_androidapp.Adapter.CustomAdapterPrevMatch;
import net.amar9059.ap_androidapp.Model.NextMatch.ResponseNextMatch;
import net.amar9059.ap_androidapp.Model.PrevMatch.EventsItem;
import net.amar9059.ap_androidapp.R;
import net.amar9059.ap_androidapp.Retrofit.ApiClient;
import net.amar9059.ap_androidapp.Retrofit.BaseApiService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class NextMatchFragment extends Fragment implements AdapterView.OnItemSelectedListener{

    private Spinner mSpinner;
    View v;
    ProgressDialog progressDialog;
    CustomAdapterNextMatch customAdapterNextMatch;
    RecyclerView recyclerView;
    int position = 0;

    public NextMatchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_next_match, container, false);
        mSpinner = v.findViewById(R.id.spinner_league);
        mSpinner.setOnItemSelectedListener(this);
        List<String> leagues = new ArrayList<String>();
        leagues.add("English Premier League");
        leagues.add("German Bundesliga");
        leagues.add("Italian Serie A");
        leagues.add("French Ligue 1");
        leagues.add("Spanish La Liga");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, leagues);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(dataAdapter);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Fetching Data...");
        progressDialog.show();

        return v;

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        // On selecting a spinner item
        String item = adapterView.getItemAtPosition(i).toString();
        String idLeague = "";
        position = mSpinner.getSelectedItemPosition();
        if (position == 0){
            idLeague = "4328";
        } if (position == 1){
            idLeague = "4331";
        } if (position == 2){
            idLeague = "4332";
        } if (position == 3){
            idLeague = "4334";
        } if (position == 4){
            idLeague = "4335";
        }

        loadNextMatch(idLeague);

        // Showing selected spinner item
        //Toast.makeText(adapterView.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();

    }

    private void loadNextMatch(final String idLeagueNext){
        progressDialog.show();
        BaseApiService baseApiService = ApiClient.client();
        baseApiService.getNextMatch(idLeagueNext).enqueue(new Callback<ResponseNextMatch>() {
            @Override
            public void onResponse(Call<ResponseNextMatch> call, Response<ResponseNextMatch> response) {
                ResponseNextMatch responseNextMatch = response.body();
                if (response.isSuccessful()){
                    if (responseNextMatch != null){
                        loadDataList(responseNextMatch.getEvents());
                    }
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseNextMatch> call, Throwable t) {
                Toast.makeText(getContext(), "Error Fecthing Data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadDataList(List<net.amar9059.ap_androidapp.Model.NextMatch.EventsItem> eventsItemListNext){
        recyclerView = v.findViewById(R.id.recycler_next_match);
        customAdapterNextMatch = new CustomAdapterNextMatch(getContext(), eventsItemListNext);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(customAdapterNextMatch);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

}
