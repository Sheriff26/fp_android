package net.amar9059.ap_androidapp.View.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import net.amar9059.ap_androidapp.Others.DetailMatches;
import net.amar9059.ap_androidapp.R;

public class DetailMatchActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView tvMatchDay;
    private ImageView imgHomeLogo;
    private TextView tvHomeTeam;
    private TextView tvHomeScore;
    private TextView tvHomeDetailGoal;
    private TextView tvHomeShoots;
    private TextView tvHomeGoalKeeper;
    private TextView tvHomeDefense;
    private TextView tvHomeMidField;
    private TextView tvHomeForward;
    private TextView tvHomeSubtitute;

    private ImageView imgAwayLogo;
    private TextView tvAwayTeam;
    private TextView tvAwayScore;
    private TextView tvAwayDetailGoal;
    private TextView tvAwayShoots;
    private TextView tvAwayGoalKeeper;
    private TextView tvAwayDefense;
    private TextView tvAwayMidField;
    private TextView tvAwayForward;
    private TextView tvAwaySubtitute;

    boolean check = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_match);

        toolbar = findViewById(R.id.id_toolbar_detail_match);
        tvMatchDay = findViewById(R.id.tv_match_date);

        imgHomeLogo = findViewById(R.id.ivHome);
        tvHomeTeam = findViewById(R.id.tvHome);
        tvHomeScore = findViewById(R.id.tvScoreHomeDet);
        tvHomeDetailGoal = findViewById(R.id.tvGoalHome);
        tvHomeShoots = findViewById(R.id.tvShotsHome);
        tvHomeGoalKeeper = findViewById(R.id.tvKeepHome);
        tvHomeDefense = findViewById(R.id.tvDefHome);
        tvHomeMidField = findViewById(R.id.tvMidHome);
        tvHomeForward = findViewById(R.id.tvForHome);
        tvHomeSubtitute = findViewById(R.id.tvSubHome);

        imgAwayLogo = findViewById(R.id.ivAway);
        tvAwayTeam = findViewById(R.id.tvAway);
        tvAwayScore = findViewById(R.id.tvScoreAwayDet);
        tvAwayDetailGoal = findViewById(R.id.tvGoalAway);
        tvAwayShoots = findViewById(R.id.tvShotsAway);
        tvAwayGoalKeeper = findViewById(R.id.tvKeepAway);
        tvAwayDefense = findViewById(R.id.tvDefAway);
        tvAwayMidField = findViewById(R.id.tvMidAway);
        tvAwayForward = findViewById(R.id.tvForAway);
        tvAwaySubtitute = findViewById(R.id.tvSubAway);

        Intent detailMatchIntent = getIntent();
        DetailMatches detailMatches = detailMatchIntent.getParcelableExtra("DetailMatch");

        String event = detailMatches.getEvent();
        String matchDay = detailMatches.getMatchDay();

        String homeTeam = detailMatches.getHomeTeam();
        String homeScore = detailMatches.getHomeScore();
        String homeGoalDetail = detailMatches.getHomeGoalDetail();
        String homeShoots = detailMatches.getHomeShoots();
        String homeGoalKeeper = detailMatches.getHomeGoalKeeper();
        String homeDefense = detailMatches.getHomeDefense();
        String homeMidfielder = detailMatches.getHomeMidfielder();
        String homeForward = detailMatches.getHomeForward();
        String homeSubtitute = detailMatches.getHomeSubtitute();

        String awayTeam = detailMatches.getAwayTeam();
        String awayScore = detailMatches.getAwayScore();
        String awayGoalDetail = detailMatches.getAwayGoalDetail();
        String awayShoots = detailMatches.getAwayShoots();
        String awayGoalKeeper = detailMatches.getAwayGoalKeeper();
        String awayDefense = detailMatches.getAwayDefense();
        String awayMidfielder = detailMatches.getAwayMidfielder();
        String awayForward = detailMatches.getAwayForward();
        String awaySubtitute = detailMatches.getAwaySubtitute();

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(event);

        tvMatchDay.setText(matchDay);
        //imgHomeLogo
        tvHomeTeam.setText(homeTeam);
        tvHomeScore.setText(homeScore);
        tvHomeDetailGoal.setText(homeGoalDetail.replace(";", "\n"));
        tvHomeShoots.setText(homeShoots);
        tvHomeGoalKeeper.setText(homeGoalKeeper.replace(";", "\n"));
        tvHomeDefense.setText(homeDefense.replace(";", "\n"));
        tvHomeMidField.setText(homeMidfielder.replace(";", "\n"));
        tvHomeForward.setText(homeForward.replace(";", "\n"));
        tvHomeSubtitute.setText(homeSubtitute.replace(";", "\n"));

        //imgAwayLogo;
        tvAwayTeam.setText(awayTeam);
        tvAwayScore.setText(awayScore);
        tvAwayDetailGoal.setText(awayGoalDetail.replace(";", "\n"));
        tvAwayShoots.setText(awayShoots);
        tvAwayGoalKeeper.setText(awayGoalKeeper.replace(";", "\n"));
        tvAwayDefense.setText(awayDefense.replace(";", "\n"));
        tvAwayMidField.setText(awayMidfielder.replace(";", "\n"));
        tvAwayForward.setText(awayForward.replace(";", "\n"));
        tvAwaySubtitute.setText(awaySubtitute.replace(";", "\n"));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.favorite_match_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.favorite_match:
                if (check){
                    //addToFavorite();
                    item.setIcon(R.drawable.ic_star_black_24dp);
                    check = false;
                } else {
                    //removeFromFavorite();
                    item.setIcon(R.drawable.ic_star_border_black_24dp);
                    check = true;
                }
                break;

        }
        return true;
    }

}
