package net.amar9059.ap_androidapp.View.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.amar9059.ap_androidapp.Others.Detail;
import net.amar9059.ap_androidapp.R;

import static android.support.constraint.Constraints.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class OverviewFragment extends Fragment {

    View view;
    TextView textViewDesc;

    public OverviewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_overview, container, false);

        textViewDesc = view.findViewById(R.id.tv_desc_team);
        Bundle bundle = getArguments();
        String deskripsi = null;
        if (bundle != null) {
            deskripsi = bundle.getString("desc");
        }
        textViewDesc.setText(deskripsi);

//        if (getArguments() != null){
//            String deskripsi = this.getArguments().getString("desc");
//            textViewDesc.setText(deskripsi);
//        }
//

//        bundle = getArguments();
//        String destTeamOverview = bundle.getString("valueDesc");
//        textViewDesc.setText(destTeamOverview);

//        destTeamOverview = getArguments().getString("descTeam");
//        textViewDesc.setText(destTeamOverview);
//        bundle = this.getArguments();
//        destTeamOverview = bundle.getString("valueDesc");

        return view;
    }

}
