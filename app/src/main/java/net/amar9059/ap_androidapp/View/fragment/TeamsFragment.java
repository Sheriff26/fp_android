package net.amar9059.ap_androidapp.View.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import net.amar9059.ap_androidapp.Adapter.CustomAdapterTeam;
import net.amar9059.ap_androidapp.Model.Teams.ResponseTeams;
import net.amar9059.ap_androidapp.Model.Teams.Teams;
import net.amar9059.ap_androidapp.R;
import net.amar9059.ap_androidapp.Retrofit.ApiClient;
import net.amar9059.ap_androidapp.Retrofit.BaseApiService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.constraint.Constraints.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class TeamsFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private Spinner mSpinner;
    View v;
    CustomAdapterTeam customAdapterTeam;
    RecyclerView recyclerView;
    ProgressDialog progressDialog;

    public TeamsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_teams, container, false);
        mSpinner = v.findViewById(R.id.spinner_league_team);
        mSpinner.setOnItemSelectedListener(this);
        List<String> leagues = new ArrayList<String>();
        leagues.add("English Premier League");
        leagues.add("German Bundesliga");
        leagues.add("Italian Serie A");
        leagues.add("French Ligue 1");
        leagues.add("Spanish La Liga");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, leagues);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(dataAdapter);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Fetching Data...");
        progressDialog.show();

        return v;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        // On selecting a spinner item
        String item = adapterView.getItemAtPosition(i).toString();
        loadTeam(item);
        //loadTeam(item);

        // Showing selected spinner item
//        Toast.makeText(adapterView.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();

    }

    private void loadTeam(final String league){
        progressDialog.show();
        BaseApiService baseApiService = ApiClient.client();
        baseApiService.getTeam(league).enqueue(new Callback<ResponseTeams>() {
            @Override
            public void onResponse(Call<ResponseTeams> call, Response<ResponseTeams> response) {
                ResponseTeams responseTeams = response.body();
                if (response.isSuccessful()){
                    if (responseTeams != null){
                        loadDataList(responseTeams.getTeams());
                    }
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseTeams> call, Throwable t) {
                Toast.makeText(getContext(), "Error Fecthing Data", Toast.LENGTH_SHORT).show();
            }
        });
    }

//    private void loadTeam(final String league) {
//        BaseApiService baseApiService = ApiClient.client();
//        baseApiService.getTeam(league).enqueue(new Callback<List<ResponseTeams>>() {
//            @Override
//            public void onResponse(Call<List<ResponseTeams>> call, Response<List<ResponseTeams>> response) {
//                loadDataList(response.body());
//            }
//
//            @Override
//            public void onFailure(Call<List<ResponseTeams>> call, Throwable t) {
//                Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

    private void loadDataList(List<Teams> teamsList) {

        recyclerView = v.findViewById(R.id.recycler_team);
        customAdapterTeam = new CustomAdapterTeam(getContext(),teamsList);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(customAdapterTeam);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

}
