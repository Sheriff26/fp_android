package net.amar9059.ap_androidapp.View.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import net.amar9059.ap_androidapp.Adapter.CustomAdapterPlayers;
import net.amar9059.ap_androidapp.Model.NextMatch.ResponseNextMatch;
import net.amar9059.ap_androidapp.Model.Players.PlayerItem;
import net.amar9059.ap_androidapp.Model.Players.ResponsePlayers;
import net.amar9059.ap_androidapp.R;
import net.amar9059.ap_androidapp.Retrofit.ApiClient;
import net.amar9059.ap_androidapp.Retrofit.BaseApiService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class PlayersFragment extends Fragment {

    View view;
    CustomAdapterPlayers customAdapterPlayers;
    RecyclerView recyclerView;


    public PlayersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_players, container, false);

        Bundle bundle = getArguments();
        String idTeam = null;
        if (bundle != null) {
            idTeam = bundle.getString("idTeam");
            loadPlayerList(idTeam);
        }

        return view;
    }

    private void loadPlayerList(final String idTeam){

        BaseApiService baseApiService = ApiClient.client();
        baseApiService.getPlayers(idTeam).enqueue(new Callback<ResponsePlayers>() {
            @Override
            public void onResponse(Call<ResponsePlayers> call, Response<ResponsePlayers> response) {
                ResponsePlayers responsePlayers = response.body();
                if (response.isSuccessful()){
                    if (responsePlayers != null){
                        loadDataList(responsePlayers.getPlayer());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponsePlayers> call, Throwable t) {
                Toast.makeText(getContext(), "Error Fecthing Data", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void loadDataList(List<PlayerItem> playerItemList){
        recyclerView = view.findViewById(R.id.recycler_player);
        customAdapterPlayers = new CustomAdapterPlayers(getContext(), playerItemList);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(customAdapterPlayers);
    }

}
