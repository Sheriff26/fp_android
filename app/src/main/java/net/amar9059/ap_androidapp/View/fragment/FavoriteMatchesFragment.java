package net.amar9059.ap_androidapp.View.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.amar9059.ap_androidapp.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteMatchesFragment extends Fragment {


    public FavoriteMatchesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorite_matches, container, false);
    }

}
