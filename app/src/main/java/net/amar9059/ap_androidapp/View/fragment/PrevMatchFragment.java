package net.amar9059.ap_androidapp.View.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.reginald.editspinner.EditSpinner;

import net.amar9059.ap_androidapp.Adapter.CustomAdapterPrevMatch;
import net.amar9059.ap_androidapp.Model.PrevMatch.EventsItem;
import net.amar9059.ap_androidapp.Model.PrevMatch.ResponsePrevMatch;
import net.amar9059.ap_androidapp.R;
import net.amar9059.ap_androidapp.Retrofit.ApiClient;
import net.amar9059.ap_androidapp.Retrofit.BaseApiService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class PrevMatchFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private Spinner mSpinner;
    View v;
    ProgressDialog progressDialog;
    CustomAdapterPrevMatch customAdapterPrevMatch;
    RecyclerView recyclerView;
    int position = 0;

    public PrevMatchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_prev_match, container, false);
        mSpinner = v.findViewById(R.id.spinner);
        mSpinner.setOnItemSelectedListener(this);
        List<String> leagues = new ArrayList<String>();
        leagues.add("English Premier League");
        leagues.add("German Bundesliga");
        leagues.add("Italian Serie A");
        leagues.add("French Ligue 1");
        leagues.add("Spanish La Liga");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, leagues);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(dataAdapter);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Fetching Data...");
        progressDialog.show();



        return v;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        // On selecting a spinner item
        String item = adapterView.getItemAtPosition(i).toString();
        String idLeague = "";
        position = mSpinner.getSelectedItemPosition();
        if (position == 0){
            idLeague = "4328";
        } if (position == 1){
            idLeague = "4331";
        } if (position == 2){
            idLeague = "4332";
        } if (position == 3){
            idLeague = "4334";
        } if (position == 4){
            idLeague = "4335";
        }

        loadPrevMatchData(idLeague);

        // Showing selected spinner item
        //Toast.makeText(adapterView.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();

    }

    private void loadPrevMatchData(final String idleague){
        progressDialog.show();
        BaseApiService baseApiService = ApiClient.client();
        baseApiService.getPrevMatch(idleague).enqueue(new Callback<ResponsePrevMatch>() {
            @Override
            public void onResponse(Call<ResponsePrevMatch> call, Response<ResponsePrevMatch> response) {
                ResponsePrevMatch responsePrevMatch = response.body();
                if (response.isSuccessful()){
                    if (responsePrevMatch != null){
                        loadDataList(responsePrevMatch.getEvents());
                    }
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponsePrevMatch> call, Throwable t) {
                Toast.makeText(getContext(), "Error Fecthing Data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadDataList(List<EventsItem> eventsItemList){
        recyclerView = v.findViewById(R.id.recycler_prev_match);
        customAdapterPrevMatch = new CustomAdapterPrevMatch(getContext(), eventsItemList);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(customAdapterPrevMatch);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
    
}
