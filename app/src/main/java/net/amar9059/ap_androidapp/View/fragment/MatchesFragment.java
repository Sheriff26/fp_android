package net.amar9059.ap_androidapp.View.fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.amar9059.ap_androidapp.Adapter.ViewPagerAdapter;
import net.amar9059.ap_androidapp.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class MatchesFragment extends Fragment {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    View v;

    public MatchesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_matches,container,false);

        tabLayout = v.findViewById(R.id.tablayout_matches);
        viewPager = v.findViewById(R.id.view_pager);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.AddFragment(new PrevMatchFragment(),"Prev Match");
        adapter.AddFragment(new NextMatchFragment(), "Next Matches");

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);


        return v;

    }

}
