package net.amar9059.ap_androidapp.View.fragment;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.amar9059.ap_androidapp.Adapter.CustomAdapterFavoriteTeams;
import net.amar9059.ap_androidapp.Adapter.CustomAdapterTeam;
import net.amar9059.ap_androidapp.Model.Teams.Teams;
import net.amar9059.ap_androidapp.R;
import net.amar9059.ap_androidapp.data.TeamsHelper;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteTeamsFragment extends Fragment {

    RecyclerView recyclerView;
    View view;
    CustomAdapterFavoriteTeams customAdapterFavoriteTeams;
    TeamsHelper teamsHelper;

    public FavoriteTeamsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.layout_teams, container, false);
        recyclerView = view.findViewById(R.id.recycler_favorite_team);

        teamsHelper = new TeamsHelper(getContext());
        customAdapterFavoriteTeams = new CustomAdapterFavoriteTeams(getContext());

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(customAdapterFavoriteTeams);

        teamsHelper.open();
        ArrayList<Teams> teamsArrayList = teamsHelper.getAllData();
        teamsHelper.close();

        customAdapterFavoriteTeams.addItem(teamsArrayList);

        return view;
    }

}
