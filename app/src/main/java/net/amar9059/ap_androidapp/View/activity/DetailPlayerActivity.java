package net.amar9059.ap_androidapp.View.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import net.amar9059.ap_androidapp.Others.DetailPlayer;
import net.amar9059.ap_androidapp.R;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class DetailPlayerActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ImageView imageView;
    private TextView tvNationality;
    private TextView tvPosition;
    private TextView tvHeight;
    private TextView tvDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_player);

        toolbar = findViewById(R.id.toolbar);
        imageView = findViewById(R.id.ivPlayer);
        tvNationality = findViewById(R.id.tvNationality);
        tvPosition = findViewById(R.id.tvPosition);
        tvHeight = findViewById(R.id.tvHeight);
        tvDescription = findViewById(R.id.tvDes);

        Intent intent = getIntent();
        DetailPlayer detailPlayer = intent.getParcelableExtra("detailPlayer");

        String imgPlayer = detailPlayer.getFotoDetail();
        String namaPlayer = detailPlayer.getNamaPlayer();
        String negaraPlayer = detailPlayer.getNegaraPlayer();
        String posisiPlayer = detailPlayer.getPosisiPlayer();
        String tinggiPlayer = detailPlayer.getTinggiPlayer();
        String deskripsiPlayer = detailPlayer.getDeskripsiPlayer();

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(namaPlayer);
        Glide.with(this)
                .load(imgPlayer)
                .transition(withCrossFade())
                .apply(new RequestOptions())
                .into(imageView);
        tvNationality.setText(negaraPlayer);
        tvPosition.setText(posisiPlayer);
        tvHeight.setText(tinggiPlayer);
        tvDescription.setText(deskripsiPlayer);

    }
}
