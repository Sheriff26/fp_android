package net.amar9059.ap_androidapp.View.activity;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import net.amar9059.ap_androidapp.Adapter.ViewPagerAdapter;
import net.amar9059.ap_androidapp.Model.Teams.Teams;
import net.amar9059.ap_androidapp.Others.Detail;
import net.amar9059.ap_androidapp.R;
import net.amar9059.ap_androidapp.View.fragment.OverviewFragment;
import net.amar9059.ap_androidapp.View.fragment.PlayersFragment;
import net.amar9059.ap_androidapp.data.TeamsHelper;

import java.util.Locale;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class DetailTeamActivity extends AppCompatActivity {

    private TabLayout tabLayoutDetailTeam;
    private ViewPager viewPagerDetailTeam;
    private ImageView imageViewLogo;
    private TextView textViewNamaTeam;
    private TextView textViewTahun;
    private Toolbar toolbar;
    Locale locale;

    private TeamsHelper favoriteDBHelper;
    private Teams favorite;

    boolean check = true;

    String image;
    String namaTeam;
    String tahunDibentuk;
    String detailTeam;
    String idTeam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_team);

        toolbar = findViewById(R.id.toolbar_detail_team);
        tabLayoutDetailTeam = findViewById(R.id.tablayout_detail_team);
        viewPagerDetailTeam = findViewById(R.id.viewpager_detail_team);
        imageViewLogo = findViewById(R.id.logo_team_detail);
        textViewNamaTeam = findViewById(R.id.nama_team_detail);
        textViewTahun = findViewById(R.id.tahun_team_detail);

        Intent intent = getIntent();
        Detail detail = intent.getParcelableExtra("DetailTeam");

        image = detail.getLogoTeam();
        namaTeam = detail.getNamaTeam();
        tahunDibentuk = detail.getTahunDibentuk();
        detailTeam = detail.getDeskripsiTeam();
        idTeam = detail.getIdTeam();

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(namaTeam);

        Glide.with(this)
                .load(image)
                .transition(withCrossFade())
                .apply(new RequestOptions().override(80,80))
                .into(imageViewLogo);

        textViewNamaTeam.setText(namaTeam);
        textViewTahun.setText(tahunDibentuk);
//        imageViewLogo.setImageResource(imageId);

        Bundle bundle = new Bundle();
        bundle.putString("desc", detailTeam);
        bundle.putString("idTeam", idTeam);

        OverviewFragment overviewFragment = new OverviewFragment();
        PlayersFragment playersFragment = new PlayersFragment();

        playersFragment.setArguments(bundle);
        overviewFragment.setArguments(bundle);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        viewPagerAdapter.AddFragment(overviewFragment, "Overview");
        viewPagerAdapter.AddFragment(playersFragment, "Players");

        viewPagerDetailTeam.setAdapter(viewPagerAdapter);
        tabLayoutDetailTeam.setupWithViewPager(viewPagerDetailTeam);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.favorite_team, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.favorite_team:
                if (check){
                    saveFavorite();
                    item.setIcon(R.drawable.ic_favorite_black_24dp);
                    check = false;
                    Toast.makeText(this, "Ditambahkan Ke Favorite", Toast.LENGTH_SHORT).show();
                } else {
                    int movie_id = getIntent().getExtras().getInt("id");
                    favoriteDBHelper = new TeamsHelper(DetailTeamActivity.this);

                    item.setIcon(R.drawable.ic_favorite_border_black_24dp);
                    check = true;
                }
                break;

        }
        return true;
    }

    public void saveFavorite(){
        favoriteDBHelper = new TeamsHelper(this);
        favorite = new Teams();

        favorite.setIdTeam(idTeam);
        favorite.setStrTeam(namaTeam);
        favorite.setStrTeamBadge(image);
        favorite.setIntFormedYear(tahunDibentuk);
        favorite.setStrDescriptionEN(detailTeam);

//        favoriteDBHelper.beginTransaction(favorite);
    }


}
