package net.amar9059.ap_androidapp.Model.Teams;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Comparator;

public class Teams implements Parcelable {

	@SerializedName("intStadiumCapacity")
	private String intStadiumCapacity;

	@SerializedName("strTeamShort")
	private String strTeamShort;

	@SerializedName("strSport")
	private String strSport;

	@SerializedName("strDescriptionCN")
	private String strDescriptionCN;

	@SerializedName("strTeamJersey")
	private String strTeamJersey;

	@SerializedName("strTeamFanart2")
	private String strTeamFanart2;

	@SerializedName("strTeamFanart3")
	private String strTeamFanart3;

	@SerializedName("strTeamFanart4")
	private String strTeamFanart4;

	@SerializedName("strStadiumDescription")
	private String strStadiumDescription;

	@SerializedName("strTeamFanart1")
	private String strTeamFanart1;

	@SerializedName("intLoved")
	private String intLoved;

	@SerializedName("idLeague")
	private String idLeague;

	@SerializedName("idSoccerXML")
	private String idSoccerXML;

	@SerializedName("strTeamLogo")
	private String strTeamLogo;

	@SerializedName("strDescriptionSE")
	private String strDescriptionSE;

	@SerializedName("strDescriptionJP")
	private String strDescriptionJP;

	@SerializedName("strDescriptionFR")
	private String strDescriptionFR;

	@SerializedName("strStadiumLocation")
	private String strStadiumLocation;

	@SerializedName("strDescriptionNL")
	private String strDescriptionNL;

	@SerializedName("strCountry")
	private String strCountry;

	@SerializedName("strRSS")
	private String strRSS;

	@SerializedName("strDescriptionRU")
	private String strDescriptionRU;

	@SerializedName("strTeamBanner")
	private String strTeamBanner;

	@SerializedName("strDescriptionNO")
	private String strDescriptionNO;

	@SerializedName("strStadiumThumb")
	private String strStadiumThumb;

	@SerializedName("strDescriptionES")
	private String strDescriptionES;

	@SerializedName("intFormedYear")
	private String intFormedYear;

	@SerializedName("strInstagram")
	private String strInstagram;

	@SerializedName("strDescriptionIT")
	private String strDescriptionIT;

	@SerializedName("idTeam")
	private String idTeam;

	@SerializedName("strDescriptionEN")
	private String strDescriptionEN;

	@SerializedName("strWebsite")
	private String strWebsite;

	@SerializedName("strYoutube")
	private String strYoutube;

	@SerializedName("strDescriptionIL")
	private String strDescriptionIL;

	@SerializedName("strLocked")
	private String strLocked;

	@SerializedName("strAlternate")
	private String strAlternate;

	@SerializedName("strTeam")
	private String strTeam;

	@SerializedName("strTwitter")
	private String strTwitter;

	@SerializedName("strDescriptionHU")
	private String strDescriptionHU;

	@SerializedName("strGender")
	private String strGender;

	@SerializedName("strDivision")
	private String strDivision;

	@SerializedName("strStadium")
	private String strStadium;

	@SerializedName("strFacebook")
	private String strFacebook;

	@SerializedName("strTeamBadge")
	private String strTeamBadge;

	@SerializedName("strDescriptionPT")
	private String strDescriptionPT;

	@SerializedName("strDescriptionDE")
	private String strDescriptionDE;

	@SerializedName("strLeague")
	private String strLeague;

	@SerializedName("strManager")
	private String strManager;

	@SerializedName("strKeywords")
	private String strKeywords;

	@SerializedName("strDescriptionPL")
	private String strDescriptionPL;

    public Teams(String intStadiumCapacity, String strTeamShort, String strSport, String strDescriptionCN, String strTeamJersey, String strTeamFanart2, String strTeamFanart3, String strTeamFanart4, String strStadiumDescription, String strTeamFanart1, String intLoved, String idLeague, String idSoccerXML, String strTeamLogo, String strDescriptionSE, String strDescriptionJP, String strDescriptionFR, String strStadiumLocation, String strDescriptionNL, String strCountry, String strRSS, String strDescriptionRU, String strTeamBanner, String strDescriptionNO, String strStadiumThumb, String strDescriptionES, String intFormedYear, String strInstagram, String strDescriptionIT, String idTeam, String strDescriptionEN, String strWebsite, String strYoutube, String strDescriptionIL, String strLocked, String strAlternate, String strTeam, String strTwitter, String strDescriptionHU, String strGender, String strDivision, String strStadium, String strFacebook, String strTeamBadge, String strDescriptionPT, String strDescriptionDE, String strLeague, String strManager, String strKeywords, String strDescriptionPL) {
        this.intStadiumCapacity = intStadiumCapacity;
        this.strTeamShort = strTeamShort;
        this.strSport = strSport;
        this.strDescriptionCN = strDescriptionCN;
        this.strTeamJersey = strTeamJersey;
        this.strTeamFanart2 = strTeamFanart2;
        this.strTeamFanart3 = strTeamFanart3;
        this.strTeamFanart4 = strTeamFanart4;
        this.strStadiumDescription = strStadiumDescription;
        this.strTeamFanart1 = strTeamFanart1;
        this.intLoved = intLoved;
        this.idLeague = idLeague;
        this.idSoccerXML = idSoccerXML;
        this.strTeamLogo = strTeamLogo;
        this.strDescriptionSE = strDescriptionSE;
        this.strDescriptionJP = strDescriptionJP;
        this.strDescriptionFR = strDescriptionFR;
        this.strStadiumLocation = strStadiumLocation;
        this.strDescriptionNL = strDescriptionNL;
        this.strCountry = strCountry;
        this.strRSS = strRSS;
        this.strDescriptionRU = strDescriptionRU;
        this.strTeamBanner = strTeamBanner;
        this.strDescriptionNO = strDescriptionNO;
        this.strStadiumThumb = strStadiumThumb;
        this.strDescriptionES = strDescriptionES;
        this.intFormedYear = intFormedYear;
        this.strInstagram = strInstagram;
        this.strDescriptionIT = strDescriptionIT;
        this.idTeam = idTeam;
        this.strDescriptionEN = strDescriptionEN;
        this.strWebsite = strWebsite;
        this.strYoutube = strYoutube;
        this.strDescriptionIL = strDescriptionIL;
        this.strLocked = strLocked;
        this.strAlternate = strAlternate;
        this.strTeam = strTeam;
        this.strTwitter = strTwitter;
        this.strDescriptionHU = strDescriptionHU;
        this.strGender = strGender;
        this.strDivision = strDivision;
        this.strStadium = strStadium;
        this.strFacebook = strFacebook;
        this.strTeamBadge = strTeamBadge;
        this.strDescriptionPT = strDescriptionPT;
        this.strDescriptionDE = strDescriptionDE;
        this.strLeague = strLeague;
        this.strManager = strManager;
        this.strKeywords = strKeywords;
        this.strDescriptionPL = strDescriptionPL;
    }

    public Teams(){

	}

	public static final Comparator<Teams> BY_NAME_ALPHABETICAL = new Comparator<Teams>() {
		@Override
		public int compare(Teams teams, Teams t1) {

			return teams.strTeam.compareTo(t1.strTeam);
		}
	};

	public void setIntStadiumCapacity(String intStadiumCapacity){
		this.intStadiumCapacity = intStadiumCapacity;
	}

	public String getIntStadiumCapacity(){
		return intStadiumCapacity;
	}

	public void setStrTeamShort(String strTeamShort){
		this.strTeamShort = strTeamShort;
	}

	public String getStrTeamShort(){
		return strTeamShort;
	}

	public void setStrSport(String strSport){
		this.strSport = strSport;
	}

	public String getStrSport(){
		return strSport;
	}

	public void setStrDescriptionCN(String strDescriptionCN){
		this.strDescriptionCN = strDescriptionCN;
	}

	public Object getStrDescriptionCN(){
		return strDescriptionCN;
	}

	public void setStrTeamJersey(String strTeamJersey){
		this.strTeamJersey = strTeamJersey;
	}

	public String getStrTeamJersey(){
		return strTeamJersey;
	}

	public void setStrTeamFanart2(String strTeamFanart2){
		this.strTeamFanart2 = strTeamFanart2;
	}

	public String getStrTeamFanart2(){
		return strTeamFanart2;
	}

	public void setStrTeamFanart3(String strTeamFanart3){
		this.strTeamFanart3 = strTeamFanart3;
	}

	public String getStrTeamFanart3(){
		return strTeamFanart3;
	}

	public void setStrTeamFanart4(String strTeamFanart4){
		this.strTeamFanart4 = strTeamFanart4;
	}

	public String getStrTeamFanart4(){
		return strTeamFanart4;
	}

	public void setStrStadiumDescription(String strStadiumDescription){
		this.strStadiumDescription = strStadiumDescription;
	}

	public String getStrStadiumDescription(){
		return strStadiumDescription;
	}

	public void setStrTeamFanart1(String strTeamFanart1){
		this.strTeamFanart1 = strTeamFanart1;
	}

	public String getStrTeamFanart1(){
		return strTeamFanart1;
	}

	public void setIntLoved(String intLoved){
		this.intLoved = intLoved;
	}

	public String getIntLoved(){
		return intLoved;
	}

	public void setIdLeague(String idLeague){
		this.idLeague = idLeague;
	}

	public String getIdLeague(){
		return idLeague;
	}

	public void setIdSoccerXML(String idSoccerXML){
		this.idSoccerXML = idSoccerXML;
	}

	public String getIdSoccerXML(){
		return idSoccerXML;
	}

	public void setStrTeamLogo(String strTeamLogo){
		this.strTeamLogo = strTeamLogo;
	}

	public String getStrTeamLogo(){
		return strTeamLogo;
	}

	public void setStrDescriptionSE(String strDescriptionSE){
		this.strDescriptionSE = strDescriptionSE;
	}

	public Object getStrDescriptionSE(){
		return strDescriptionSE;
	}

	public void setStrDescriptionJP(String strDescriptionJP){
		this.strDescriptionJP = strDescriptionJP;
	}

	public Object getStrDescriptionJP(){
		return strDescriptionJP;
	}

	public void setStrDescriptionFR(String strDescriptionFR){
		this.strDescriptionFR = strDescriptionFR;
	}

	public Object getStrDescriptionFR(){
		return strDescriptionFR;
	}

	public void setStrStadiumLocation(String strStadiumLocation){
		this.strStadiumLocation = strStadiumLocation;
	}

	public String getStrStadiumLocation(){
		return strStadiumLocation;
	}

	public void setStrDescriptionNL(String strDescriptionNL){
		this.strDescriptionNL = strDescriptionNL;
	}

	public Object getStrDescriptionNL(){
		return strDescriptionNL;
	}

	public void setStrCountry(String strCountry){
		this.strCountry = strCountry;
	}

	public String getStrCountry(){
		return strCountry;
	}

	public void setStrRSS(String strRSS){
		this.strRSS = strRSS;
	}

	public String getStrRSS(){
		return strRSS;
	}

	public void setStrDescriptionRU(String strDescriptionRU){
		this.strDescriptionRU = strDescriptionRU;
	}

	public Object getStrDescriptionRU(){
		return strDescriptionRU;
	}

	public void setStrTeamBanner(String strTeamBanner){
		this.strTeamBanner = strTeamBanner;
	}

	public String getStrTeamBanner(){
		return strTeamBanner;
	}

	public void setStrDescriptionNO(String strDescriptionNO){
		this.strDescriptionNO = strDescriptionNO;
	}

	public String getStrDescriptionNO(){
		return strDescriptionNO;
	}

	public void setStrStadiumThumb(String strStadiumThumb){
		this.strStadiumThumb = strStadiumThumb;
	}

	public String getStrStadiumThumb(){
		return strStadiumThumb;
	}

	public void setStrDescriptionES(String strDescriptionES){
		this.strDescriptionES = strDescriptionES;
	}

	public Object getStrDescriptionES(){
		return strDescriptionES;
	}

	public void setIntFormedYear(String intFormedYear){
		this.intFormedYear = intFormedYear;
	}

	public String getIntFormedYear(){
		return intFormedYear;
	}

	public void setStrInstagram(String strInstagram){
		this.strInstagram = strInstagram;
	}

	public String getStrInstagram(){
		return strInstagram;
	}

	public void setStrDescriptionIT(String strDescriptionIT){
		this.strDescriptionIT = strDescriptionIT;
	}

	public String getStrDescriptionIT(){
		return strDescriptionIT;
	}

	public void setIdTeam(String idTeam){
		this.idTeam = idTeam;
	}

	public String getIdTeam(){
		return idTeam;
	}

	public void setStrDescriptionEN(String strDescriptionEN){
		this.strDescriptionEN = strDescriptionEN;
	}

	public String getStrDescriptionEN(){
		return strDescriptionEN;
	}

	public void setStrWebsite(String strWebsite){
		this.strWebsite = strWebsite;
	}

	public String getStrWebsite(){
		return strWebsite;
	}

	public void setStrYoutube(String strYoutube){
		this.strYoutube = strYoutube;
	}

	public String getStrYoutube(){
		return strYoutube;
	}

	public void setStrDescriptionIL(String strDescriptionIL){
		this.strDescriptionIL = strDescriptionIL;
	}

	public Object getStrDescriptionIL(){
		return strDescriptionIL;
	}

	public void setStrLocked(String strLocked){
		this.strLocked = strLocked;
	}

	public String getStrLocked(){
		return strLocked;
	}

	public void setStrAlternate(String strAlternate){
		this.strAlternate = strAlternate;
	}

	public String getStrAlternate(){
		return strAlternate;
	}

	public void setStrTeam(String strTeam){
		this.strTeam = strTeam;
	}

	public String getStrTeam(){
		return strTeam;
	}

	public void setStrTwitter(String strTwitter){
		this.strTwitter = strTwitter;
	}

	public String getStrTwitter(){
		return strTwitter;
	}

	public void setStrDescriptionHU(String strDescriptionHU){
		this.strDescriptionHU = strDescriptionHU;
	}

	public Object getStrDescriptionHU(){
		return strDescriptionHU;
	}

	public void setStrGender(String strGender){
		this.strGender = strGender;
	}

	public String getStrGender(){
		return strGender;
	}

	public void setStrDivision(String strDivision){
		this.strDivision = strDivision;
	}

	public String getStrDivision(){
		return strDivision;
	}

	public void setStrStadium(String strStadium){
		this.strStadium = strStadium;
	}

	public String getStrStadium(){
		return strStadium;
	}

	public void setStrFacebook(String strFacebook){
		this.strFacebook = strFacebook;
	}

	public String getStrFacebook(){
		return strFacebook;
	}

	public void setStrTeamBadge(String strTeamBadge){
		this.strTeamBadge = strTeamBadge;
	}

	public String getStrTeamBadge(){
		return strTeamBadge;
	}

	public void setStrDescriptionPT(String strDescriptionPT){
		this.strDescriptionPT = strDescriptionPT;
	}

	public Object getStrDescriptionPT(){
		return strDescriptionPT;
	}

	public void setStrDescriptionDE(String strDescriptionDE){
		this.strDescriptionDE = strDescriptionDE;
	}

	public String getStrDescriptionDE(){
		return strDescriptionDE;
	}

	public void setStrLeague(String strLeague){
		this.strLeague = strLeague;
	}

	public String getStrLeague(){
		return strLeague;
	}

	public void setStrManager(String strManager){
		this.strManager = strManager;
	}

	public String getStrManager(){
		return strManager;
	}

	public void setStrKeywords(String strKeywords){
		this.strKeywords = strKeywords;
	}

	public String getStrKeywords(){
		return strKeywords;
	}

	public void setStrDescriptionPL(String strDescriptionPL){
		this.strDescriptionPL = strDescriptionPL;
	}

	public String  getStrDescriptionPL(){
		return strDescriptionPL;
	}

	@Override
 	public String toString(){
		return 
			"Teams{" +
			"intStadiumCapacity = '" + intStadiumCapacity + '\'' + 
			",strTeamShort = '" + strTeamShort + '\'' + 
			",strSport = '" + strSport + '\'' + 
			",strDescriptionCN = '" + strDescriptionCN + '\'' + 
			",strTeamJersey = '" + strTeamJersey + '\'' + 
			",strTeamFanart2 = '" + strTeamFanart2 + '\'' + 
			",strTeamFanart3 = '" + strTeamFanart3 + '\'' + 
			",strTeamFanart4 = '" + strTeamFanart4 + '\'' + 
			",strStadiumDescription = '" + strStadiumDescription + '\'' + 
			",strTeamFanart1 = '" + strTeamFanart1 + '\'' + 
			",intLoved = '" + intLoved + '\'' + 
			",idLeague = '" + idLeague + '\'' + 
			",idSoccerXML = '" + idSoccerXML + '\'' + 
			",strTeamLogo = '" + strTeamLogo + '\'' + 
			",strDescriptionSE = '" + strDescriptionSE + '\'' + 
			",strDescriptionJP = '" + strDescriptionJP + '\'' + 
			",strDescriptionFR = '" + strDescriptionFR + '\'' + 
			",strStadiumLocation = '" + strStadiumLocation + '\'' + 
			",strDescriptionNL = '" + strDescriptionNL + '\'' + 
			",strCountry = '" + strCountry + '\'' + 
			",strRSS = '" + strRSS + '\'' + 
			",strDescriptionRU = '" + strDescriptionRU + '\'' + 
			",strTeamBanner = '" + strTeamBanner + '\'' + 
			",strDescriptionNO = '" + strDescriptionNO + '\'' + 
			",strStadiumThumb = '" + strStadiumThumb + '\'' + 
			",strDescriptionES = '" + strDescriptionES + '\'' + 
			",intFormedYear = '" + intFormedYear + '\'' + 
			",strInstagram = '" + strInstagram + '\'' + 
			",strDescriptionIT = '" + strDescriptionIT + '\'' + 
			",idTeam = '" + idTeam + '\'' + 
			",strDescriptionEN = '" + strDescriptionEN + '\'' + 
			",strWebsite = '" + strWebsite + '\'' + 
			",strYoutube = '" + strYoutube + '\'' + 
			",strDescriptionIL = '" + strDescriptionIL + '\'' + 
			",strLocked = '" + strLocked + '\'' + 
			",strAlternate = '" + strAlternate + '\'' + 
			",strTeam = '" + strTeam + '\'' + 
			",strTwitter = '" + strTwitter + '\'' + 
			",strDescriptionHU = '" + strDescriptionHU + '\'' + 
			",strGender = '" + strGender + '\'' + 
			",strDivision = '" + strDivision + '\'' + 
			",strStadium = '" + strStadium + '\'' + 
			",strFacebook = '" + strFacebook + '\'' + 
			",strTeamBadge = '" + strTeamBadge + '\'' + 
			",strDescriptionPT = '" + strDescriptionPT + '\'' + 
			",strDescriptionDE = '" + strDescriptionDE + '\'' + 
			",strLeague = '" + strLeague + '\'' + 
			",strManager = '" + strManager + '\'' + 
			",strKeywords = '" + strKeywords + '\'' + 
			",strDescriptionPL = '" + strDescriptionPL + '\'' + 
			"}";
		}


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.intStadiumCapacity);
        dest.writeString(this.strTeamShort);
        dest.writeString(this.strSport);
        dest.writeString(this.strDescriptionCN);
        dest.writeString(this.strTeamJersey);
        dest.writeString(this.strTeamFanart2);
        dest.writeString(this.strTeamFanart3);
        dest.writeString(this.strTeamFanart4);
        dest.writeString(this.strStadiumDescription);
        dest.writeString(this.strTeamFanart1);
        dest.writeString(this.intLoved);
        dest.writeString(this.idLeague);
        dest.writeString(this.idSoccerXML);
        dest.writeString(this.strTeamLogo);
        dest.writeString(this.strDescriptionSE);
        dest.writeString(this.strDescriptionJP);
        dest.writeString(this.strDescriptionFR);
        dest.writeString(this.strStadiumLocation);
        dest.writeString(this.strDescriptionNL);
        dest.writeString(this.strCountry);
        dest.writeString(this.strRSS);
        dest.writeString(this.strDescriptionRU);
        dest.writeString(this.strTeamBanner);
        dest.writeString(this.strDescriptionNO);
        dest.writeString(this.strStadiumThumb);
        dest.writeString(this.strDescriptionES);
        dest.writeString(this.intFormedYear);
        dest.writeString(this.strInstagram);
        dest.writeString(this.strDescriptionIT);
        dest.writeString(this.idTeam);
        dest.writeString(this.strDescriptionEN);
        dest.writeString(this.strWebsite);
        dest.writeString(this.strYoutube);
        dest.writeString(this.strDescriptionIL);
        dest.writeString(this.strLocked);
        dest.writeString(this.strAlternate);
        dest.writeString(this.strTeam);
        dest.writeString(this.strTwitter);
        dest.writeString(this.strDescriptionHU);
        dest.writeString(this.strGender);
        dest.writeString(this.strDivision);
        dest.writeString(this.strStadium);
        dest.writeString(this.strFacebook);
        dest.writeString(this.strTeamBadge);
        dest.writeString(this.strDescriptionPT);
        dest.writeString(this.strDescriptionDE);
        dest.writeString(this.strLeague);
        dest.writeString(this.strManager);
        dest.writeString(this.strKeywords);
        dest.writeString(this.strDescriptionPL);
    }

    protected Teams(Parcel in) {
        this.intStadiumCapacity = in.readString();
        this.strTeamShort = in.readString();
        this.strSport = in.readString();
        this.strDescriptionCN = in.readString();
        this.strTeamJersey = in.readString();
        this.strTeamFanart2 = in.readString();
        this.strTeamFanart3 = in.readString();
        this.strTeamFanart4 = in.readString();
        this.strStadiumDescription = in.readString();
        this.strTeamFanart1 = in.readString();
        this.intLoved = in.readString();
        this.idLeague = in.readString();
        this.idSoccerXML = in.readString();
        this.strTeamLogo = in.readString();
        this.strDescriptionSE = in.readString();
        this.strDescriptionJP = in.readString();
        this.strDescriptionFR = in.readString();
        this.strStadiumLocation = in.readString();
        this.strDescriptionNL = in.readString();
        this.strCountry = in.readString();
        this.strRSS = in.readString();
        this.strDescriptionRU = in.readString();
        this.strTeamBanner = in.readString();
        this.strDescriptionNO = in.readString();
        this.strStadiumThumb = in.readString();
        this.strDescriptionES = in.readString();
        this.intFormedYear = in.readString();
        this.strInstagram = in.readString();
        this.strDescriptionIT = in.readString();
        this.idTeam = in.readString();
        this.strDescriptionEN = in.readString();
        this.strWebsite = in.readString();
        this.strYoutube = in.readString();
        this.strDescriptionIL = in.readString();
        this.strLocked = in.readString();
        this.strAlternate = in.readString();
        this.strTeam = in.readString();
        this.strTwitter = in.readString();
        this.strDescriptionHU = in.readString();
        this.strGender = in.readString();
        this.strDivision = in.readString();
        this.strStadium = in.readString();
        this.strFacebook = in.readString();
        this.strTeamBadge = in.readString();
        this.strDescriptionPT = in.readString();
        this.strDescriptionDE = in.readString();
        this.strLeague = in.readString();
        this.strManager = in.readString();
        this.strKeywords = in.readString();
        this.strDescriptionPL = in.readString();
    }

    public static final Parcelable.Creator<Teams> CREATOR = new Parcelable.Creator<Teams>() {
        @Override
        public Teams createFromParcel(Parcel source) {
            return new Teams(source);
        }

        @Override
        public Teams[] newArray(int size) {
            return new Teams[size];
        }
    };
}