package net.amar9059.ap_androidapp.Model.Players;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class ResponsePlayers{

	@SerializedName("player")
	private List<PlayerItem> player;

	public void setPlayer(List<PlayerItem> player){
		this.player = player;
	}

	public List<PlayerItem> getPlayer(){
		return player;
	}

	@Override
 	public String toString(){
		return 
			"ResponsePlayers{" + 
			"player = '" + player + '\'' + 
			"}";
		}
}