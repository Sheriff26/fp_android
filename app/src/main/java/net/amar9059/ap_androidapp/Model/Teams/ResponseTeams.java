package net.amar9059.ap_androidapp.Model.Teams;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class ResponseTeams{

	@SerializedName("teams")
	private List<Teams> teams;

	public void setTeams(List<Teams> teams){
		this.teams = teams;
	}

	public List<Teams> getTeams(){
		return teams;
	}

	@Override
 	public String toString(){
		return 
			"ResponseTeams{" + 
			"teams = '" + teams + '\'' + 
			"}";
		}
}