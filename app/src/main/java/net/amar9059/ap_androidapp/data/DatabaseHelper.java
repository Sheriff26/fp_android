package net.amar9059.ap_androidapp.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static android.provider.BaseColumns._ID;
import static net.amar9059.ap_androidapp.data.Favorites.FavoriteEntry.COLUMN_DETAIL_TEAM;
import static net.amar9059.ap_androidapp.data.Favorites.FavoriteEntry.COLUMN_LOGO_TEAM;
import static net.amar9059.ap_androidapp.data.Favorites.FavoriteEntry.COLUMN_NAMATEAM;
import static net.amar9059.ap_androidapp.data.Favorites.FavoriteEntry.COLUMN_TAHUN_DIBENTUK;
import static net.amar9059.ap_androidapp.data.Favorites.FavoriteEntry.COLUMN_TEAMID;
import static net.amar9059.ap_androidapp.data.Favorites.FavoriteEntry.TABLE_NAME;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static String DATABASE_NAME = "favorite";

    private static final int DATABASE_VERSION = 1;

    public static String CREATE_TABLE_FAVORITE = "CREATE TABLE " + TABLE_NAME +
            " ("+_ID+ " INTEGER PRIMARY KEY AUTOINCREMENT," +
            COLUMN_TEAMID + " TEXT NOT NULL, " +
            COLUMN_NAMATEAM + " TEXT NOT NULL, " +
            COLUMN_TAHUN_DIBENTUK + " TEXT NOT NULL, " +
            COLUMN_LOGO_TEAM + " TEXT NOT NULL, " +
            COLUMN_DETAIL_TEAM + " TEXT NOT NULL );";

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_FAVORITE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }

}
