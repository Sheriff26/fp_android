package net.amar9059.ap_androidapp.data;

import android.provider.BaseColumns;

public final class Favorites {

    private Favorites() {
    }

    public static final class FavoriteEntry implements BaseColumns{
        public static final String TABLE_NAME = "favorite";
        public static final String COLUMN_TEAMID = "teamid";
        public static final String COLUMN_NAMATEAM = "namateam";
        public static final String COLUMN_TAHUN_DIBENTUK = "tahundibentuk";
        public static final String COLUMN_LOGO_TEAM = "logoteam";
        public static final String COLUMN_DETAIL_TEAM = "detailteam";
    }

}
