package net.amar9059.ap_androidapp.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import net.amar9059.ap_androidapp.Model.Teams.Teams;

import java.util.ArrayList;

import static android.provider.BaseColumns._ID;
import static net.amar9059.ap_androidapp.data.Favorites.FavoriteEntry.COLUMN_DETAIL_TEAM;
import static net.amar9059.ap_androidapp.data.Favorites.FavoriteEntry.COLUMN_LOGO_TEAM;
import static net.amar9059.ap_androidapp.data.Favorites.FavoriteEntry.COLUMN_NAMATEAM;
import static net.amar9059.ap_androidapp.data.Favorites.FavoriteEntry.COLUMN_TAHUN_DIBENTUK;
import static net.amar9059.ap_androidapp.data.Favorites.FavoriteEntry.COLUMN_TEAMID;
import static net.amar9059.ap_androidapp.data.Favorites.FavoriteEntry.TABLE_NAME;

public class TeamsHelper {

    private Context context;
    private DatabaseHelper dataBaseHelper;

    private SQLiteDatabase database;

    public TeamsHelper(Context context){
        this.context = context;
    }

    public TeamsHelper open() throws SQLException {
        dataBaseHelper = new DatabaseHelper(context);
        database = dataBaseHelper.getWritableDatabase();
        return this;
    }

    public void close(){
        dataBaseHelper.close();
    }

    public ArrayList<Teams> getDataByName(String nama){
        String result = "";
        Cursor cursor = database.query(TABLE_NAME,null,COLUMN_NAMATEAM+" LIKE ?",new String[]{nama},null,null,_ID + " ASC",null);
        cursor.moveToFirst();
        ArrayList<Teams> arrayList = new ArrayList<>();
        Teams teams;
        if (cursor.getCount()>0) {
            do {
                teams = new Teams();
                teams.setIdTeam(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_TEAMID)));
                teams.setStrTeam(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAMATEAM)));
                teams.setIntFormedYear(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_TAHUN_DIBENTUK)));
                teams.setStrTeamBadge(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_LOGO_TEAM)));
                teams.setStrDescriptionEN(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DETAIL_TEAM)));

                arrayList.add(teams);
                cursor.moveToNext();

            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }

    public ArrayList<Teams> getAllData(){
        Cursor cursor = database.query(TABLE_NAME,null,null,null,null,null,COLUMN_TEAMID+ " ASC",null);
        cursor.moveToFirst();
        ArrayList<Teams> arrayList = new ArrayList<>();
        Teams teams;
        if (cursor.getCount()>0) {
            do {
                teams = new Teams();
                teams.setIdTeam(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_TEAMID)));
                teams.setStrTeam(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAMATEAM)));
                teams.setIntFormedYear(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_TAHUN_DIBENTUK)));
                teams.setStrTeamBadge(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_LOGO_TEAM)));
                teams.setStrDescriptionEN(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DETAIL_TEAM)));


                arrayList.add(teams);
                cursor.moveToNext();


            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }

    public long insert(Teams teams){
        ContentValues initialValues =  new ContentValues();
        initialValues.put(COLUMN_TEAMID, teams.getIdTeam());
        initialValues.put(COLUMN_NAMATEAM, teams.getStrTeam());
        initialValues.put(COLUMN_TAHUN_DIBENTUK, teams.getIntFormedYear());
        initialValues.put(COLUMN_LOGO_TEAM, teams.getStrTeamBadge());
        initialValues.put(COLUMN_DETAIL_TEAM, teams.getStrDescriptionEN());

        return database.insert(TABLE_NAME, null, initialValues);
    }

    public void beginTransaction(){
        database.beginTransaction();
    }

    public void setTransactionSuccess(){
        database.setTransactionSuccessful();
    }

    public void endTransaction(){
        database.endTransaction();
    }

    public void insertTransaction(Teams teams){
        String sql = "INSERT INTO "+TABLE_NAME+" ("+COLUMN_TEAMID+", "+COLUMN_NAMATEAM+", " +
                ""+COLUMN_TAHUN_DIBENTUK+", "+COLUMN_LOGO_TEAM+", "+COLUMN_DETAIL_TEAM
                +") VALUES (?, ?)";
        SQLiteStatement stmt = database.compileStatement(sql);
        stmt.bindString(1, teams.getIdTeam());
        stmt.bindString(2, teams.getStrTeam());
        stmt.bindString(2, teams.getIntFormedYear());
        stmt.bindString(2, teams.getStrTeamBadge());
        stmt.bindString(2, teams.getStrDescriptionEN());
        stmt.execute();
        stmt.clearBindings();

    }

    public int update(Teams teams){
        ContentValues args = new ContentValues();
        args.put(COLUMN_TEAMID, teams.getIdTeam());
        args.put(COLUMN_NAMATEAM, teams.getStrTeam());
        args.put(COLUMN_TAHUN_DIBENTUK, teams.getIntFormedYear());
        args.put(COLUMN_LOGO_TEAM, teams.getStrTeamBadge());
        args.put(COLUMN_DETAIL_TEAM, teams.getStrDescriptionEN());

        return database.update(TABLE_NAME, args, _ID + "= '" + teams.getIdTeam() + "'", null);
    }


    public int delete(int id){
        return database.delete(TABLE_NAME, _ID + " = '"+id+"'", null);
    }

}
