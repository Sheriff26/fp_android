package net.amar9059.ap_androidapp.Others;

import android.os.Parcel;
import android.os.Parcelable;

public class Detail implements Parcelable {

    private String idTeam;
    private String namaTeam;
    private String tahunDibentuk;
    private String idLeague;
    private String managerTeam;
    private String deskripsiTeam;
    private String logoTeam;

    public Detail(String idTeam, String namaTeam, String tahunDibentuk, String idLeague, String managerTeam, String deskripsiTeam, String logoTeam) {
        this.idTeam = idTeam;
        this.namaTeam = namaTeam;
        this.tahunDibentuk = tahunDibentuk;
        this.idLeague = idLeague;
        this.managerTeam = managerTeam;
        this.deskripsiTeam = deskripsiTeam;
        this.logoTeam = logoTeam;
    }

    public String getIdTeam() {
        return idTeam;
    }

    public String getNamaTeam() {
        return namaTeam;
    }

    public String getTahunDibentuk() {
        return tahunDibentuk;
    }

    public String getIdLeague() {
        return idLeague;
    }

    public String getManagerTeam() {
        return managerTeam;
    }

    public String getDeskripsiTeam() {
        return deskripsiTeam;
    }

    public String getLogoTeam() {
        return logoTeam;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(idTeam);
        parcel.writeString(namaTeam);
        parcel.writeString(tahunDibentuk);
        parcel.writeString(idLeague);
        parcel.writeString(managerTeam);
        parcel.writeString(deskripsiTeam);
        parcel.writeString(logoTeam);

    }

    public Detail (Parcel parcel){
        idTeam = parcel.readString();
        namaTeam = parcel.readString();
        tahunDibentuk = parcel.readString();
        idLeague = parcel.readString();
        managerTeam = parcel.readString();
        deskripsiTeam = parcel.readString();
        logoTeam = parcel.readString();
    }

    public static final Parcelable.Creator<Detail> CREATOR = new Parcelable.Creator<Detail>(){

        @Override
        public Detail createFromParcel(Parcel parcel) {
            return new Detail(parcel);
        }

        @Override
        public Detail[] newArray(int size) {
            return new Detail[0];
        }
    };

    @Override
    public int describeContents() {
        return hashCode();
    }
}
