package net.amar9059.ap_androidapp.Others;

import android.os.Parcel;
import android.os.Parcelable;

public class DetailMatches implements Parcelable {

    private String event;
    private String matchDay;

    private String homeLogo;
    private String homeTeam;
    private String homeScore;
    private String homeGoalDetail;
    private String homeShoots;
    private String homeGoalKeeper;
    private String homeDefense;
    private String homeMidfielder;
    private String homeForward;
    private String homeSubtitute;

    private String awayLogo;
    private String awayTeam;
    private String awayScore;
    private String awayGoalDetail;
    private String awayShoots;
    private String awayGoalKeeper;
    private String awayDefense;
    private String awayMidfielder;
    private String awayForward;
    private String awaySubtitute;

    public DetailMatches(String event, String matchDay, String homeTeam, String homeScore, String homeGoalDetail, String homeShoots, String homeGoalKeeper, String homeDefense, String homeMidfielder, String homeForward, String homeSubtitute, String awayTeam, String awayScore, String awayGoalDetail, String awayShoots, String awayGoalKeeper, String awayDefense, String awayMidfielder, String awayForward, String awaySubtitute) {
        this.event = event;
        this.matchDay = matchDay;
        this.homeTeam = homeTeam;
        this.homeScore = homeScore;
        this.homeGoalDetail = homeGoalDetail;
        this.homeShoots = homeShoots;
        this.homeGoalKeeper = homeGoalKeeper;
        this.homeDefense = homeDefense;
        this.homeMidfielder = homeMidfielder;
        this.homeForward = homeForward;
        this.homeSubtitute = homeSubtitute;
        this.awayTeam = awayTeam;
        this.awayScore = awayScore;
        this.awayGoalDetail = awayGoalDetail;
        this.awayShoots = awayShoots;
        this.awayGoalKeeper = awayGoalKeeper;
        this.awayDefense = awayDefense;
        this.awayMidfielder = awayMidfielder;
        this.awayForward = awayForward;
        this.awaySubtitute = awaySubtitute;
    }

    public String getEvent() {
        return event;
    }

    public String getMatchDay() {
        return matchDay;
    }

    public String getHomeTeam() {
        return homeTeam;
    }

    public String getHomeScore() {
        return homeScore;
    }

    public String getHomeGoalDetail() {
        return homeGoalDetail;
    }

    public String getHomeShoots() {
        return homeShoots;
    }

    public String getHomeGoalKeeper() {
        return homeGoalKeeper;
    }

    public String getHomeDefense() {
        return homeDefense;
    }

    public String getHomeMidfielder() {
        return homeMidfielder;
    }

    public String getHomeForward() {
        return homeForward;
    }

    public String getHomeSubtitute() {
        return homeSubtitute;
    }

    public String getAwayTeam() {
        return awayTeam;
    }

    public String getAwayScore() {
        return awayScore;
    }

    public String getAwayGoalDetail() {
        return awayGoalDetail;
    }

    public String getAwayShoots() {
        return awayShoots;
    }

    public String getAwayGoalKeeper() {
        return awayGoalKeeper;
    }

    public String getAwayDefense() {
        return awayDefense;
    }

    public String getAwayMidfielder() {
        return awayMidfielder;
    }

    public String getAwayForward() {
        return awayForward;
    }

    public String getAwaySubtitute() {
        return awaySubtitute;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(event);
        parcel.writeString(matchDay);

        parcel.writeString(homeTeam);
        parcel.writeString(homeScore);
        parcel.writeString(homeGoalDetail);
        parcel.writeString(homeShoots);
        parcel.writeString(homeGoalKeeper);
        parcel.writeString(homeDefense);
        parcel.writeString(homeMidfielder);
        parcel.writeString(homeForward);
        parcel.writeString(homeSubtitute);

        parcel.writeString(awayTeam);
        parcel.writeString(awayScore);
        parcel.writeString(awayGoalDetail);
        parcel.writeString(awayShoots);
        parcel.writeString(awayGoalKeeper);
        parcel.writeString(awayDefense);
        parcel.writeString(awayMidfielder);
        parcel.writeString(awayForward);
        parcel.writeString(awaySubtitute);
    }

    protected DetailMatches(Parcel in) {
        event = in.readString();
        matchDay = in.readString();

        homeTeam = in.readString();
        homeScore = in.readString();
        homeGoalDetail = in.readString();
        homeShoots = in.readString();
        homeGoalKeeper = in.readString();
        homeDefense = in.readString();
        homeMidfielder = in.readString();
        homeForward = in.readString();
        homeSubtitute = in.readString();

        awayTeam = in.readString();
        awayScore = in.readString();
        awayGoalDetail = in.readString();
        awayShoots = in.readString();
        awayGoalKeeper = in.readString();
        awayDefense = in.readString();
        awayMidfielder = in.readString();
        awayForward = in.readString();
        awaySubtitute = in.readString();
    }

    public static final Creator<DetailMatches> CREATOR = new Creator<DetailMatches>() {
        @Override
        public DetailMatches createFromParcel(Parcel in) {
            return new DetailMatches(in);
        }

        @Override
        public DetailMatches[] newArray(int size) {
            return new DetailMatches[0];
        }
    };

    @Override
    public int describeContents() {
        return hashCode();
    }
}
