package net.amar9059.ap_androidapp.Others;

import android.os.Parcel;
import android.os.Parcelable;

public class DetailPlayer implements Parcelable {

    private String namaPlayer;
    private String fotoDetail;
    private String negaraPlayer;
    private String posisiPlayer;
    private String tinggiPlayer;
    private String deskripsiPlayer;

    public DetailPlayer(String namaPlayer, String fotoDetail, String negaraPlayer, String posisiPlayer, String tinggiPlayer, String deskripsiPlayer) {
        this.namaPlayer = namaPlayer;
        this.fotoDetail = fotoDetail;
        this.negaraPlayer = negaraPlayer;
        this.posisiPlayer = posisiPlayer;
        this.tinggiPlayer = tinggiPlayer;
        this.deskripsiPlayer = deskripsiPlayer;
    }

    public String getNamaPlayer() {
        return namaPlayer;
    }

    public String getFotoDetail() {
        return fotoDetail;
    }

    public String getNegaraPlayer() {
        return negaraPlayer;
    }

    public String getPosisiPlayer() {
        return posisiPlayer;
    }

    public String getTinggiPlayer() {
        return tinggiPlayer;
    }

    public String getDeskripsiPlayer() {
        return deskripsiPlayer;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(namaPlayer);
        parcel.writeString(fotoDetail);
        parcel.writeString(negaraPlayer);
        parcel.writeString(posisiPlayer);
        parcel.writeString(tinggiPlayer);
        parcel.writeString(deskripsiPlayer);
    }

    protected DetailPlayer(Parcel in) {
        namaPlayer = in.readString();
        fotoDetail = in.readString();
        negaraPlayer = in.readString();
        posisiPlayer = in.readString();
        tinggiPlayer = in.readString();
        deskripsiPlayer = in.readString();
    }

    public static final Creator<DetailPlayer> CREATOR = new Creator<DetailPlayer>() {
        @Override
        public DetailPlayer createFromParcel(Parcel in) {
            return new DetailPlayer(in);
        }

        @Override
        public DetailPlayer[] newArray(int size) {
            return new DetailPlayer[0];
        }
    };

    @Override
    public int describeContents() {
        return hashCode();
    }
}
