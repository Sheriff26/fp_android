package net.amar9059.ap_androidapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.amar9059.ap_androidapp.Model.PrevMatch.EventsItem;
import net.amar9059.ap_androidapp.Others.DetailMatches;
import net.amar9059.ap_androidapp.R;
import net.amar9059.ap_androidapp.View.activity.DetailMatchActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CustomAdapterPrevMatch extends RecyclerView.Adapter<CustomAdapterPrevMatch.CustomViewHolder> {

    private Context context;
    private List<EventsItem> eventsItemList;

    public CustomAdapterPrevMatch(Context context, List<EventsItem> eventsItemList) {
        this.context = context;
        this.eventsItemList = eventsItemList;
    }

    @NonNull
    @Override
    public CustomAdapterPrevMatch.CustomViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.layout_prev_match, viewGroup, false);
        return new CustomViewHolder(view,context,eventsItemList);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomAdapterPrevMatch.CustomViewHolder customViewHolder, int i) {

        String time = eventsItemList.get(i).getStrTime();
        String times = time.substring(0, 5);

        customViewHolder.tvDateMatch.setText(convertTime(eventsItemList.get(i).getDateEvent()) +", " +times);
        customViewHolder.tvHomeTeam.setText(eventsItemList.get(i).getStrHomeTeam());
        customViewHolder.tvHomeScore.setText(eventsItemList.get(i).getIntHomeScore());
        customViewHolder.tvAwayTeam.setText(eventsItemList.get(i).getStrAwayTeam());
        customViewHolder.tvAwayScore.setText(eventsItemList.get(i).getIntAwayScore());
    }

    @Override
    public int getItemCount() {
        return eventsItemList.size();
    }

    private String convertTime(String time) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format1 = new SimpleDateFormat("EEE, dd MMM yyyy",Locale.UK);
        java.util.Date date = null;

        try {
            date = format.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String convertedDate = format1.format(date);

        return convertedDate;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView tvDateMatch;
        TextView tvHomeTeam;
        TextView tvHomeScore;
        TextView tvAwayTeam;
        TextView tvAwayScore;

        private Context context;
        private List<EventsItem> eventsItemList;

        public CustomViewHolder(@NonNull View itemView, Context context, List<EventsItem> eventsItemList) {
            super(itemView);
            this.context = context;
            this.eventsItemList = eventsItemList;
            itemView.setOnClickListener(this);

            tvDateMatch = itemView.findViewById(R.id.tv_date_match);
            tvHomeTeam = itemView.findViewById(R.id.tv_home_team);
            tvHomeScore = itemView.findViewById(R.id.tv_score_home);
            tvAwayTeam = itemView.findViewById(R.id.tv_away_team);
            tvAwayScore = itemView.findViewById(R.id.tv_score_away);

        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            EventsItem eventsItem = this.eventsItemList.get(position);
            String event = eventsItem.getStrEvent();
            String matchDay = convertTime(eventsItem.getDateEvent());

            String homeTeam = eventsItem.getStrHomeTeam();
            String homeScore = eventsItem.getIntHomeScore();
            String homeGoalDetail = eventsItem.getStrHomeGoalDetails();
            String homeShoots = eventsItem.getIntHomeShots();
            String homeGoalKeeper = eventsItem.getStrHomeLineupGoalkeeper();
            String homeDefense =  eventsItem.getStrHomeLineupDefense();
            String homeMidfielder = eventsItem.getStrHomeLineupMidfield();
            String homeForward = eventsItem.getStrHomeLineupForward();
            String homeSubtitute = eventsItem.getStrHomeLineupSubstitutes();

            String awayTeam = eventsItem.getStrAwayTeam();
            String awayScore = eventsItem.getIntAwayScore();
            String awayGoalDetail = eventsItem.getStrAwayGoalDetails();
            String awayShoots = eventsItem.getIntAwayShots();
            String awayGoalKeeper = eventsItem.getStrAwayLineupGoalkeeper();
            String awayDefense =  eventsItem.getStrAwayLineupDefense();
            String awayMidfielder = eventsItem.getStrAwayLineupMidfield();
            String awayForward = eventsItem.getStrAwayLineupForward();
            String awaySubtitute = eventsItem.getStrAwayLineupSubstitutes();

            DetailMatches detailMatches = new DetailMatches(event, matchDay, homeTeam, homeScore, homeGoalDetail
                    , homeShoots, homeGoalKeeper, homeDefense, homeMidfielder, homeForward, homeSubtitute
                    , awayTeam, awayScore, awayGoalDetail, awayShoots, awayGoalKeeper, awayDefense
                    , awayMidfielder, awayForward, awaySubtitute);
            Intent detailMatchIntent = new Intent(this.context, DetailMatchActivity.class);
            detailMatchIntent.putExtra("DetailMatch", detailMatches);
            this.context.startActivity(detailMatchIntent);

        }
    }

}
