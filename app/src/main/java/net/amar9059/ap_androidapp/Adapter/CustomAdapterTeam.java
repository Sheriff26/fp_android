package net.amar9059.ap_androidapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import net.amar9059.ap_androidapp.Others.Detail;
import net.amar9059.ap_androidapp.View.activity.DetailTeamActivity;
import net.amar9059.ap_androidapp.Model.Teams.Teams;
import net.amar9059.ap_androidapp.R;
import net.amar9059.ap_androidapp.View.fragment.OverviewFragment;

import java.util.List;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class CustomAdapterTeam extends RecyclerView.Adapter<CustomAdapterTeam.CustomViewHolder> {

    private Context context;
    private List<Teams> teamsList;

    public CustomAdapterTeam(Context context, List<Teams> teamsList) {
        this.context = context;
        this.teamsList = teamsList;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public final View view;
        ImageView imgLogoTeam;
        TextView tvNamaTeam;
        List<Teams> teamsList;
        Context context;

        public CustomViewHolder(@NonNull View itemView, Context context, List<Teams> teamsList) {
            super(itemView);
            this.context = context;
            this.teamsList = teamsList;
            view = itemView;
            view.setOnClickListener(this);

            imgLogoTeam = view.findViewById(R.id.logo_team);
            tvNamaTeam = view.findViewById(R.id.tv_team);

        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            Teams teams = this.teamsList.get(position);

            String namaTeam = teams.getStrTeam();
            String tahunTeam = teams.getIntFormedYear();
            String idteam = teams.getIdTeam();
            String idLeague = teams.getIdLeague();
            String detailTeam = teams.getStrDescriptionEN();
            String managerTeam = teams.getStrManager();
            String logoTeam = teams.getStrTeamBadge();

            Detail detail = new Detail(idteam, namaTeam, tahunTeam, idLeague, managerTeam, detailTeam, logoTeam);
            Intent detailTeamIntent = new Intent(this.context, DetailTeamActivity.class);
            detailTeamIntent.putExtra("DetailTeam", detail);

//            imgLogoTeam.buildDrawingCache();
//            Bitmap image = imgLogoTeam.getDrawingCache();
//            Bundle bundle = new Bundle();
//            bundle.putParcelable("logoTeam", image);
//            bundle.putString("namaTeam", teams.getStrTeam());
//            bundle.putString("tahunTeam", teams.getIntFormedYear());
//            bundle.putString("detailTeam", teams.getStrDescriptionEN());
//
//            detailTeamIntent.putExtra("Detail", detal);
//
//            bundle.putString("descTeam",teams.getStrDescriptionEN());
//            OverviewFragment overviewFragment = new OverviewFragment();
//            overviewFragment.setArguments(bundle);
//
//            detailTeamIntent.putExtra("descriptionTeam", teams.getStrDescriptionEN());
//            detailTeamIntent.putExtra("namaTeam", teams.getStrTeam());
//            detailTeamIntent.putExtra("tahunTeam", teams.getIntFormedYear());
//
//            detailTeamIntent.putExtra("Detail", teamsList.get(position));

//            detailTeamIntent.putExtra("descTeam", teams.getStrDescriptionEN());
            this.context.startActivity(detailTeamIntent);
        }
    }

    @NonNull
    @Override
    public CustomAdapterTeam.CustomViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.layout_teams, viewGroup, false);
        return new CustomViewHolder(view, context, teamsList);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomAdapterTeam.CustomViewHolder customViewHolder, int i) {
        Glide.with(context)
                .load(teamsList.get(i).getStrTeamBadge())
                .transition(withCrossFade())
                .apply(new RequestOptions().override(80,80))
                .into(customViewHolder.imgLogoTeam);
//        customViewHolder.imgLogoTeam.setImageResource(Integer.parseInt(responseTeamsList.get(i).getTeams().get(i).getStrTeamLogo()));
        customViewHolder.tvNamaTeam.setText(teamsList.get(i).getStrTeam());
    }

    @Override
    public int getItemCount() {
        return teamsList.size();
    }

}
