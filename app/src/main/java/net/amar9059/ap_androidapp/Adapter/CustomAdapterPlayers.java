package net.amar9059.ap_androidapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import net.amar9059.ap_androidapp.Model.Players.PlayerItem;
import net.amar9059.ap_androidapp.Others.DetailPlayer;
import net.amar9059.ap_androidapp.R;
import net.amar9059.ap_androidapp.View.activity.DetailPlayerActivity;

import java.util.List;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class CustomAdapterPlayers extends RecyclerView.Adapter<CustomAdapterPlayers.CustomViewHolder> {

    Context context;
    List<PlayerItem> playerItemList;

    public CustomAdapterPlayers(Context context, List<PlayerItem> playerItemList) {
        this.context = context;
        this.playerItemList = playerItemList;
    }

    @NonNull
    @Override
    public CustomAdapterPlayers.CustomViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.layout_player, viewGroup, false);
        return new CustomViewHolder(view, context, playerItemList);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomAdapterPlayers.CustomViewHolder customViewHolder, int i) {
        Glide.with(context)
                .load(playerItemList.get(i).getStrThumb())
                .transition(withCrossFade())
                .apply(new RequestOptions().override(60,60))
                .into(customViewHolder.imgFotoPlayer);
        customViewHolder.namaPlayer.setText(playerItemList.get(i).getStrPlayer());
        customViewHolder.posisiPlayer.setText(playerItemList.get(i).getStrPosition());
    }

    @Override
    public int getItemCount() {
        return playerItemList.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView imgFotoPlayer;
        TextView namaPlayer;
        TextView posisiPlayer;
        List<PlayerItem> playerItemList;
        Context context;

        public CustomViewHolder(@NonNull View itemView, Context context, List<PlayerItem> playerItemList) {
            super(itemView);
            this.context = context;
            this.playerItemList = playerItemList;
            itemView.setOnClickListener(this);

            imgFotoPlayer = itemView.findViewById(R.id.foto_player);
            namaPlayer = itemView.findViewById(R.id.nama_player);
            posisiPlayer = itemView.findViewById(R.id.posisi_player);

        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            PlayerItem playerItem = this.playerItemList.get(position);

            String namaPlayer = playerItem.getStrPlayer();
            String fotoDetail = playerItem.getStrFanart1();
            String negaraPlayer = playerItem.getStrNationality();
            String posisiPlayer = playerItem.getStrPosition();
            String tinggiPlayer = playerItem.getStrHeight();
            String deskripsiPlayer = playerItem.getStrDescriptionEN();

            DetailPlayer detailPlayer = new DetailPlayer(namaPlayer, fotoDetail, negaraPlayer, posisiPlayer, tinggiPlayer, deskripsiPlayer);
            Intent detailPlayerIntent = new Intent(this.context, DetailPlayerActivity.class);
            detailPlayerIntent.putExtra("detailPlayer", detailPlayer);
            this.context.startActivity(detailPlayerIntent);

        }
    }
}
