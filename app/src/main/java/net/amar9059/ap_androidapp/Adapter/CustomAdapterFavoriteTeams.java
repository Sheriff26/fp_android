package net.amar9059.ap_androidapp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import net.amar9059.ap_androidapp.Model.Teams.Teams;
import net.amar9059.ap_androidapp.R;

import java.util.ArrayList;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class CustomAdapterFavoriteTeams extends RecyclerView.Adapter<CustomAdapterFavoriteTeams.CustomViewHolder> {

    private ArrayList<Teams> mData = new ArrayList<>();
    private Context context;
    private LayoutInflater layoutInflater;

    public CustomAdapterFavoriteTeams(Context context) {
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @NonNull
    @Override
    public CustomAdapterFavoriteTeams.CustomViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_teams, viewGroup, false);

        return new CustomViewHolder(view);
    }

    public void addItem(ArrayList<Teams> mData) {
        this.mData = mData;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull CustomAdapterFavoriteTeams.CustomViewHolder customViewHolder, int i) {
        Glide.with(context)
                .load(mData.get(i).getStrTeamBadge())
                .transition(withCrossFade())
                .apply(new RequestOptions().override(80,80))
                .into(customViewHolder.imgLogoTeam);
//        customViewHolder.imgLogoTeam.setImageResource(Integer.parseInt(responseTeamsList.get(i).getTeams().get(i).getStrTeamLogo()));
        customViewHolder.tvNamaTeam.setText(mData.get(i).getStrTeam());
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        ImageView imgLogoTeam;
        TextView tvNamaTeam;

        public CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            imgLogoTeam = itemView.findViewById(R.id.logo_team);
            tvNamaTeam = itemView.findViewById(R.id.tv_team);
        }
    }
}
