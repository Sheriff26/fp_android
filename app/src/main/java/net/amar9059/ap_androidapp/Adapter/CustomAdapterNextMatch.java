package net.amar9059.ap_androidapp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.amar9059.ap_androidapp.Model.NextMatch.EventsItem;
import net.amar9059.ap_androidapp.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class CustomAdapterNextMatch extends RecyclerView.Adapter<CustomAdapterNextMatch.CustomViewHolder> {

    private Context context;
    private List<EventsItem> eventsNextItemList;

    public CustomAdapterNextMatch(Context context, List<EventsItem> eventsNextItemList) {
        this.context = context;
        this.eventsNextItemList = eventsNextItemList;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        public final View view;
        TextView tvDateMatchNext;
        TextView tvHomeTeamNext;
        TextView tvAwayTeamNext;

        public CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;

            tvDateMatchNext = view.findViewById(R.id.tv_date_match_next);
            tvHomeTeamNext = view.findViewById(R.id.tv_home_team_next);
            tvAwayTeamNext = view.findViewById(R.id.tv_away_team_next);

        }
    }

    @NonNull
    @Override
    public CustomAdapterNextMatch.CustomViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.layout_next_match, viewGroup, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomAdapterNextMatch.CustomViewHolder customViewHolder, int i) {

        String time = eventsNextItemList.get(i).getStrTime();
        String times = time.substring(0, 5);

        customViewHolder.tvDateMatchNext.setText(convertTime(eventsNextItemList.get(i).getDateEvent()) +", " +times);
        customViewHolder.tvHomeTeamNext.setText(eventsNextItemList.get(i).getStrHomeTeam());
        customViewHolder.tvAwayTeamNext.setText(eventsNextItemList.get(i).getStrAwayTeam());

    }

    @Override
    public int getItemCount() {
        return eventsNextItemList.size();
    }

    private String convertTime(String time) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format1 = new SimpleDateFormat("EEE, dd MMM yyyy",Locale.UK);
        java.util.Date date = null;

        try {
            date = format.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String convertedDate = format1.format(date);

        return convertedDate;
    }

}
